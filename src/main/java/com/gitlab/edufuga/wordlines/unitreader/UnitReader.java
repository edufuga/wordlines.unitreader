package com.gitlab.edufuga.wordlines.unitreader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class UnitReader {
    private final Path unitsFolder;

    public UnitReader(Path unitsFolder) {
        this.unitsFolder = unitsFolder;
    }

    public UnitReader(String unitsFolder) {
        this(Paths.get(unitsFolder));
    }

    public List<String> read(String word) throws IOException {
        List<Path> units = findConnections(word);

        // TODO: Maybe read the file contents and show the date of the union. For now I keep it simple.

        return units.stream().map(Path::getFileName).map(Objects::toString).collect(Collectors.toList());
    }

    private List<Path> findConnections(String word) throws IOException {
        Path wordFolder = unitsFolder.resolve(word);

        if (Files.notExists(wordFolder)) {
            return Collections.emptyList();
        }

        return Files.list(wordFolder).collect(Collectors.toList());
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("Usage: Units folder and 'from' word to read the units for.");
            return;
        }

        String units = args[0];
        String from = args[1];

        UnitReader unitReader = new UnitReader(units);
        unitReader.read(from).forEach(System.out::println);
    }
}
